import { ForbiddenException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { SignInDto, SignUpDto, ResponseDto } from './auth.dto';
import { hash, compare } from 'bcrypt';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersServise: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async signUp(dto: SignUpDto): Promise<ResponseDto> {
    dto.password = await hash(dto.password, 13);
    const user = await this.usersServise.create(dto);
    const jwtToken = await this.jwtService.signAsync({ sub: user.id });
    return { jwtToken };
  }
  async signIn(dto: SignInDto): Promise<ResponseDto> {
    const user = await this.usersServise.getOne(dto.username);
    if (!user) {
      throw new ForbiddenException('User with this name is exist');
    }
    const isCorrectPassword = await compare(dto.password, user.password);
    if (!isCorrectPassword) {
      throw new ForbiddenException('Password is incorrect');
    }
    const jwtToken = await this.jwtService.signAsync({ sub: user.id });
    return { jwtToken: jwtToken };
  }
}
