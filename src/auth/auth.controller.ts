import { Body, Controller, Post } from '@nestjs/common';
import { SignInDto, SignUpDto, ResponseDto } from './auth.dto';
import { AuthService } from './auth.service';

@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('auth/signUp')
  async signUp(@Body() body: SignUpDto): Promise<ResponseDto> {
    return this.authService.signUp(body);
  }

  @Post('auth/signIn')
  async signIn(@Body() body: SignInDto): Promise<ResponseDto> {
    return this.authService.signIn(body);
  }
}
